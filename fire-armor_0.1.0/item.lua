--item.lua

local fireArmor = table.deepcopy(data.raw["armor"]["heavy-armor"]) 

fireArmor.name = "fire-armor"
fireArmor.icons = {
  {
   icon = fireArmor.icon,
   tint = {r=1,g=0,b=0,a=0.3}
  },
}

fireArmor.resistences = {
	{
		type = "physical",
		decrease = 0,
		percent = 100
	},
	{
		type = "exsplosion",
		decrease = 0,
		percent = 100
	},
	{
		type = "acid"
		decrease = 0,
		percent = 100
	},
	{
		type = "fire"
		decrease = 0,
      		percent = 100
	}
}
local recipy = table.deepcopy(data.raw["recipe"]{heavy-armor"])
recipe.enabled = true
recipe.name "fire-armor"
recipe.ingredients = {{"copper-plate",200},{"steel-plate",50}}
recipe.result = "fire-armor"

data:exstend{fireArmor,recipe}
