
function initPlayer(player)
	if player.character == nil then return end
	if global == nil then
		global = {}
	end
	if global.donePlayers == nil then
		global.donePlayers = {}
	end
	if global.donePlayers[player] ~= nil then return end
	global.donePlayers[player] = true

	player.get_inventory(defines.inventory.character_main).clear()
	player.get_inventory(defines.inventory.character_armor).clear()
	player.get_inventory(defines.inventory.character_guns).clear()
	player.get_inventory(defines.inventory.character_ammo).clear()

	local items = {
		-- resources
		{"wood", 150},
		{"iron-ore", 500},
		{"copper-ore", 300},
		{"iron-gear-wheel", 100},
		{"electronic-circuit", 100},
		-- belts
		{"fast-transport-belt", 2000},
		{"fast-underground-belt", 200},
		{"fast-splitter", 50},
		-- pipes
		{"pipe-to-ground", 5},
		{"pipe", 5},
		-- other logistic
		{"inserter", 500},
		{"wooden-chest", 50},
		{"construction-robot", 10},
		-- buildings
		{"steel-furnace", 200},
		{"assembling-machine-2", 200},
		{"electric-mining-drill", 200},
		-- electric poles
		{"small-electric-pole ", 400},
		{"medium-electric-pole", 200},
		{"big-electric-pole", 50},
		{"substation", 25}.
		--power production
		{"heat-exchanger", 10},
		{"nuclear-reactor", 2},
		{"uranium-fuel-cell", 50},
		{"offshore-pump", 5},
		--ammo
		{"uranium-rounds-magazine", 200},

	}
	for _, v in pairs(items) do
		player.insert{name = v[1], count = v[2]}
	end

	local armorInventory = player.get_inventory(defines.inventory.character_armor)
	armorInventory.insert("power-armor-mk2")
	local armorGrid = armorInventory.find_item_stack("power-armor-mk2").grid

	local equipment = {
		"fusion-reactor-equipment",
		"exoskeleton-equipment",
		"personal-roboport-equipment-mk2",
	}
	for _, v in pairs(equipment) do
		armorGrid.put{name = v}
	end

	player.get_inventory(defines.inventory.character_guns).insert{name = "submachine-gun", count = 1}
	player.get_inventory(defines.inventory.character_ammo).insert{name = "uranium-rounds-magazine", count = 200}
end

function onPlayerJoined(event)
	local player = game.players[event.player_index]
	initPlayer(player)
end

script.on_event({defines.events.on_player_joined_game, defines.events.on_player_created}, onPlayerJoined)

function onModInit()
	if remote.interfaces["freeplay"] then
		remote.call("freeplay", "set_disable_crashsite", true)
		remote.call("freeplay", "set_skip_intro", true)
	end
end

script.on_init(onModInit)
