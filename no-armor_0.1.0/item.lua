--item.lua

local UsefullArmor = table.deepcopy(data.raw["armor"]["heavy-armor"]) 

 UsefullArmor name = "usefull-armor"
UsefullArmor icons = {
  {
   icon = UsefullArmor.icon,
   tint = {r=1,g=0,b=0,a=0.3}
  },
}

UsefullArmor.resistences = {
	{
		type = "physical",
		decrease = 0,
		percent = 0
	},
	{
		type = "exsplosion",
		decrease = 0,
		percent = 0
	},
	{
		type = "acid"
		decrease = 0,
		percent = 0
	},
	{
		type = "fire"
		decrease = 0,
      		percent = 0
	}
}
local recipy = table.deepcopy(data.raw["recipe"]{heavy-armor"])
recipe.enabled = true
recipe.name "UsefullArmor" 
recipe.ingredients = {{"copper-plate",200},{"steel-plate",50}}
recipe.result = "UsefullArmor"

data:exstend{UsefullArmor,recipe}
